/*
 gsf_adapter.js: Adapts "GSF decoder" backend to generic WebAudio/ScriptProcessor player.

    version 1.1
 	copyright (C) 2018-2023 Juergen Wothke


 note: copy/paste of respective 2sf_adapter (see webDS)

 LICENSE

   This library is distributed under the Mozilla Public License version 2.0. A copy of
   the license is available in the distributed LICENSE file.
*/
class GSFBackendAdapter extends EmsHEAP16BackendAdapter {
	constructor(modlandMode)
	{
		super(backend_gsf.Module, 2, new SimpleFileMapper(backend_gsf.Module));
		
		this.ensureReadyNotification();
	}
	
	loadMusicData(sampleRate, path, filename, data, options)
	{
		filename = this._getFilename(path, filename);

		let ret = this.Module.ccall('emu_load_file', 'number',
							['string', 'number', 'number', 'number', 'number', 'number'],
							[ filename, 0, 0, ScriptNodePlayer.getWebAudioSampleRate(), -999, false]);

		if (ret == 0) {
			this._setupOutputResampling(sampleRate);
		}
		return ret;
	}

	evalTrackOptions(options)
	{
		super.evalTrackOptions(options);

		let boostVolume = (options && options.boostVolume) ? options.boostVolume : 0;
		this.Module.ccall('emu_set_boost', 'number', ['number'], [boostVolume]);

		// no subsongs in this format
		return 0;
	}

	getSongInfoMeta()
	{
		return {
			title: String,
			artist: String,
			game: String,
			year: String,
			genre: String,
			copyright: String,
			psfby: String
		};
	}

	updateSongInfo(filename)
	{
		let result = this._songInfo;
		
		let numAttr = 7;
		let ret = this.Module.ccall('emu_get_track_info', 'number');

		let array = this.Module.HEAP32.subarray(ret>>2, (ret>>2)+numAttr);
		result.title = this.Module.Pointer_stringify(array[0]);
		if (!result.title.length) result.title = filename.replace(/^.*[\\\/]/, '');
		result.artist = this.Module.Pointer_stringify(array[1]);
		result.game = this.Module.Pointer_stringify(array[2]);
		result.year = this.Module.Pointer_stringify(array[3]);
		result.genre = this.Module.Pointer_stringify(array[4]);
		result.copyright = this.Module.Pointer_stringify(array[5]);
		result.psfby = this.Module.Pointer_stringify(array[6]);
	}
};
