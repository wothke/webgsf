let songs = [
		"Gameboy Sound Format/Hitoshi Sakimoto/Final Fantasy Tactics Advance/01 main theme.minigsf",
		"Gameboy Sound Format/Barry Leitch/Batman - Rise Of Sin Tzu/01 main theme.minigsf",
		"Gameboy Sound Format/Julius/Jazz Jackrabbit Advance/main title.minigsf",
		"Gameboy Sound Format/Masaru Setsumaru/Chu Chu Rocket/01 title.minigsf",
		"Gameboy Sound Format/Ichiro Shimakura/Mario Party Advance/01 intro.minigsf",
		"Gameboy Sound Format/Abe Hideki/Puyo Pop Fever/puyo pop - 01 title.minigsf",
		"Gameboy Sound Format/Tommy Tallarico/Earthworm Jim/title.minigsf",
	];

class GSFDisplayAccessor extends DisplayAccessor {
	constructor(doGetSongInfo)
	{
		super(doGetSongInfo);
	}

	getDisplayTitle() 		{ return "webGSF";}
	getDisplaySubtitle() 	{ return "mGBA in action";}
	getDisplayLine1() { return this.getSongInfo().title +" ("+this.getSongInfo().artist+")";}
	getDisplayLine2() { return this.getSongInfo().copyright; }
	getDisplayLine3() { return ""; }
};

class GSFControls extends BasicControls {
	constructor(containerId, songs, doParseUrl, onNewTrackCallback, enableSeek, enableSpeedTweak, current)
	{
		super(containerId, songs, doParseUrl, onNewTrackCallback, enableSeek, enableSpeedTweak, current);
	}

	_addSong(filename)
	{
		if (filename.indexOf(".gsflib") == -1)
		{
			super._addSong(filename);
		}
	}
};

class Main {
	constructor()
	{
		this._backend;
		this._playerWidget;
		this._songDisplay;
	}

	_doOnUpdate()
	{
		if (typeof this._lastId != 'undefined')
		{
			window.cancelAnimationFrame(this._lastId);	// prevent duplicate chains
		}
		this._animate();

		this._songDisplay.redrawSongInfo();
	}

	_animate()
	{
		this._songDisplay.redrawSpectrum();
		this._playerWidget.animate()

		this._lastId = window.requestAnimationFrame(this._animate.bind(this));
	}

	_doOnTrackEnd()
	{
		this._playerWidget.playNextSong();
	}

	_playSongIdx(i)
	{
		this._playerWidget.playSongIdx(i);
	}

	run()
	{
		let preloadFiles = [];	// no need for preload

		// note: with WASM this may not be immediately ready
		this._backend = new GSFBackendAdapter(true);
		this._backend.setProcessorBufSize(2048);

		ScriptNodePlayer.initialize(this._backend, this._doOnTrackEnd.bind(this), preloadFiles, true, undefined)
		.then((msg) => {

			let makeOptionsFromUrl = function(someSong) {
					let isLocal = someSong.startsWith("/tmp/") || someSong.startsWith("music/");
					someSong = isLocal ? someSong : window.location.protocol + "//ftp.modland.com/pub/modules/" + someSong;

					return [someSong, {}];
				};

			this._playerWidget = new GSFControls("controls", songs, makeOptionsFromUrl, this._doOnUpdate.bind(this), false, true);

			this._songDisplay = new SongDisplay(new GSFDisplayAccessor((function(){return this._playerWidget.getSongInfo();}.bind(this) )),
								[0x505050,0xffffff,0x404040,0xffffff], 1, 0.5);

			this._playerWidget.playNextSong();
		});
	}
}